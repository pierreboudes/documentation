# Suivre les activités à travers la fédération

## Avec Mastodon/Pleroma

### Suivre un groupe via Mastodon/Pleroma

Vous avez la possibilité de voir l'activité d'un groupe d'une instance Mobilizon dans votre flux Mastodon/Pleroma. Pour cela vous devez avoir un compte sur le réseau Mastodon/Pleroma, puis&nbsp;:

  1. faire une recherche du groupe dans la barre de recherche de Mastodon/Pleroma
  1. cliquer sur l'icône pour suivre le groupe

![image de la page d'inscription](../../images/mastodon-suivre-groupe1.png)

Ou&nbsp;:

  1. faire une recherche du groupe dans la barre de recherche de Mastodon/Pleroma
  1. cliquer sur le compte
  1. cliquer sur le bouton **Suivre**
	
![image de la page d'inscription](../../images/mastodon-suivre-groupe.gif)

!!! note
    Un compte administrateur du groupe peut rejeter une demande de suivi&nbsp: si le bouton **Suivre** réapparaît, il est possible que votre demande ait été rejetée.
